# Tutos Wordpress

Tutoriel pour utiliser et personnaliser Wordpress.

## License

[CC0 Creative Commons Zero](./LICENSE.md)

Provient de l'[atelier animé au Pic par JPeGfr](https://framagit.org/JPeGfr/Amination_WordPress_Pic).


## Installer une démo

Installer un site wordpress en local en utilisant docker.

```bash
cp env.dist .env
docker-compose up -d
```

Puis aller à l'adresse http://0.0.0.0:8080 pour installer le site Wordpress.
